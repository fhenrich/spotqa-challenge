# spotqa-challenge

> SpotQA Recruitment Challenge

## ToDo

- Task 2 _"My website is slow, how can I make it faster?"_
- Using visualisation (e.g., plotting the data using your favourite library), determine if there is a correlation between employee age, income and happiness.
- [BONUS] Can you determine which managers have happier teams, and which have sadder teams?

## Extra ToDo

- Improve UX with v-binds
- Add tests
- Search functionality
- Better separation of code
- Experiment with routing and state management

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
yarn build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
