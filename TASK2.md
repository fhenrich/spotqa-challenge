# _"My website is slow, how can I make it faster?"_

There can be many reasons to slow down a web application, and sometimes you can figure out at first glance, but more often than not there are multiple causes, and many not as obvious and straightforward as they seem. I try to digest it by using a ToDo/Wishlist of possible causes and respective solutions.

- Too many network calls
- CSS critical path improperly set up
- Assets and scripts bundled/minified incorrectly
- Long running functions
- Caching
- Not rendering on server-side first
- CDN static content
- Optimize static content, media for the web
- Misuse of front-end framework features or on it's entirety


## Too many network calls

...

## CSS critical path improperly set up

## Assets and scripts bundled/minified incorrectly

## Long running functions

## Caching

## Not rendering on server-side first

## CDN static content

## Optimize static content, media for the web

## Misuse of front-end framework features or on it's entirety

