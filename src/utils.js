import axios from "axios"

/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
function isObject(item) {
  return item && typeof item === "object" && !Array.isArray(item)
}

/**
 * Deep merge two objects.
 * @param target
 * @param source
 * @return merged
 */
export function deepMerge(target, source) {
  let merged = { ...target }

  if (isObject(target) && isObject(source))
    Object.keys(source).forEach(key => {
      if (isObject(source[key]))
        if (!(key in target))
          merged = { ...merged, ...{ [key]: source[key] } }
        else 
          merged[key] = deepMerge(target[key], source[key])
      else 
        merged = { ...merged, ...{ [key]: source[key] } }
    })

  return merged
}

/**
 * Sort array of objects by property in ascending or descending order.
 * @param property
 * @param {boolean} reverse
 * @return {Number} result
 */
export function dynamicSort(property, reverse) {
  const sortOrder = reverse ? -1 : 1
  return (a, b) => {
    const result =
      a[property] < b[property] ? -1 :
      a[property] > b[property] ?  1 : 0
    return result * sortOrder
  }
}

/**
 * Convert raw data from API to an object tree.
 * @param {string} records
 * @return {object} dataObject
 */
export function dataConversion(records) {
  let dataObject = {}
  let fields = []
  let lines = records.split("\n")
  lines.shift()

  lines.forEach((el, i) => {
    fields = lines[i].split(",")
    dataObject = deepMerge(dataObject, {
      divisions: {
        [fields[0]]: {
          teams: {
            [fields[1]]: {
              managers: {
                [fields[2]]: {
                  employees: {
                    [fields[3]]: {
                      id: parseInt(fields[3]),
                      firstName: fields[4],
                      lastName: fields[5],
                      birthdate: fields[6],
                      happiness: fields[7],
                      income: parseInt(fields[8])
                    }
                  }
                }
              }
            }
          }
        }
      }
    })
  })

  return dataObject
}

/**
 * Flattens the internal data object to a flat list of objects for Vue to iterate.
 * @param obj
 * @param arrToDisplay
 * @param row
 * @return {Array} dataTodisplay
 */
export function dataToDisplay(obj, arrToDisplay, row) {
  arrToDisplay = arrToDisplay || []
  row = row || {}

  Object.keys(obj).forEach(key => {
    if(obj[key].hasOwnProperty("id"))
      return
    
    if (key) {
      if (obj[key].hasOwnProperty("teams")) 
        row.divisionId = parseInt(key)

      if (obj[key].hasOwnProperty("managers")) 
        row.teamId = parseInt(key)

      if (obj[key].hasOwnProperty("employees")) 
        row.managerId = parseInt(key)

      if (key === "employees")
        for (const employee in obj[key]) {
          row = { ...row, ...obj[key][employee] }
          arrToDisplay.push({ ...row })
        }

      dataToDisplay(obj[key], arrToDisplay, row)
    }
  })

  return arrToDisplay
}

/**
 * Retrieves raw data from API as a Promise
 * @param {number} start
 * @param {number} end
 * @return {Promise} retrievedData
 */
export async function retrieveData(start, end) {
  start = start || 0
  end = end || 99

  return await axios(
    `http://163.172.172.242:4242/challenge/survey/${start}/${end}`
  )
}
